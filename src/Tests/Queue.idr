module Tests.Queue

import Data.Vect
import Test.Unit.Assertions
import Queue

testEnqueueDequeue : IO ()
testEnqueueDequeue = do
  let q = empty
  let q = enqueue q 1
  let q = enqueue q 2
  let q = enqueue q 3
  let (e, q) = dequeue q
  assertEquals e 1
  let (e, q) = dequeue q
  assertEquals e 2
  let (e, q) = dequeue q
  assertEquals e 3
  pure ()

testLength : IO ()
testLength = do
  assertEquals (length (the (Queue 0 Int) empty)) 0
  assertEquals (length (enqueue empty 1)) 1
  assertEquals (length (enqueue (enqueue empty 1) 1)) 2
  pure ()



testAppend : IO ()
testAppend = do
  let q1 = fromVect [0, 1]
  let q2 = fromVect [2, 3]
  let q = append q1 q2
  let (e, q) = dequeue q
  assertEquals e 0
  let (e, q) = dequeue q
  assertEquals e 1
  let (e, q) = dequeue q
  assertEquals e 2
  let (e, q) = dequeue q
  assertEquals e 3
  pure ()


export
test : IO ()
test = do
  testEnqueueDequeue
  testLength
  testAppend
  pure ()
