module BoundedQueue
import Data.Vect

export
data BoundedQueue : Nat -> Type -> Type where
  MkQueue: {l, m: Nat} -> (front : Vect l a) -> (rear : Vect m a) -> (p: Nat) -> BoundedQueue (l + m + p) a

%default total

export
empty : BoundedQueue n a
empty {n=n} = MkQueue [] [] n

export
isEmpty : BoundedQueue n a -> Bool
isEmpty (MkQueue [] [] _) = True
isEmpty _ = False

falseNotTrue : False = True -> Void
falseNotTrue Refl impossible

export
decIsEmpty: (q: BoundedQueue n a) -> Dec (isEmpty q = True)
decIsEmpty (MkQueue  []     []    _) = Yes Refl
decIsEmpty (MkQueue (_::_)  _     _) = No falseNotTrue
decIsEmpty (MkQueue  []    (_::_) _) = No falseNotTrue

export
isFull : BoundedQueue n a -> Bool
isFull (MkQueue _ _ Z)     = True
isFull (MkQueue _ _ (S _)) = False

export
decIsFull : (q: BoundedQueue n a) -> Dec (isFull q = True)
decIsFull (MkQueue _ _ Z)     = Yes Refl
decIsFull (MkQueue _ _ (S _)) = No falseNotTrue

export
weaken : BoundedQueue n a -> (w: Nat) -> BoundedQueue (n + w) a
weaken (MkQueue {l} {m} front rear pad) w =
  rewrite sym $ plusAssociative (l + m) pad w in
  MkQueue front rear (pad + w)

export
length : BoundedQueue n a -> Fin (S n)
length {n = l + m + pad} (MkQueue {l=l} {m=m} front rear pad) =
  let f: Fin (S (l + m)) = last in
  let f: Fin (S (l + m) + pad) = weakenN pad f in
  f

export
isEmptyLengthZero: (q: BoundedQueue n a) -> (p: isEmpty q = True) -> length q = FZ
isEmptyLengthZero (MkQueue  []     []    _) p = Refl
isEmptyLengthZero (MkQueue (_::_)   _    _) p = void $ falseNotTrue p
isEmptyLengthZero (MkQueue  []    (_::_) _) p = void $ falseNotTrue p

export
lengthZeroIsEmpty: (q: BoundedQueue n a) -> (p: length q = 0) -> isEmpty q = True
lengthZeroIsEmpty (MkQueue  []     []    _) _ = Refl
lengthZeroIsEmpty (MkQueue (_::_)  _     _) p = void $ (negEqSym FZNotFS) p
lengthZeroIsEmpty (MkQueue  []    (_::_) _) p =  void $ (negEqSym FZNotFS) p


export
buffer: BoundedQueue n a -> Fin (S n)
buffer (MkQueue _ _ Z) = FZ
buffer {n = l + m + (S pad)} (MkQueue {l} {m} fs rs (S pad)) =
  let f: Fin (S pad) = last in
  let f: Fin (S pad + (l + m)) = weakenN (l + m) f in
  let f: Fin (S (l + m + pad)) = rewrite plusCommutative (l + m) pad in f in
  let f: Fin (l + m + S pad) = rewrite sym $ plusSuccRightSucc (l + m) pad in f in
  FS f

export
isFullBufferZero: (q: BoundedQueue n a) -> (p: isFull q = True) -> buffer q = 0
isFullBufferZero (MkQueue _ _    Z ) _ = Refl
isFullBufferZero (MkQueue _ _ (S _)) p = void $ falseNotTrue p

export
bufferZeroIsFull: (q: BoundedQueue n a) -> (p: buffer q = 0) -> isFull q = True
bufferZeroIsFull (MkQueue _ _    Z ) _ = Refl
bufferZeroIsFull (MkQueue _ _ (S _)) p = void $ (negEqSym FZNotFS) p


-- TODO: decide name
enqueuePrf : (q: BoundedQueue n a) -> a -> {prf: Not (isFull q = True)} -> BoundedQueue n a
enqueuePrf (MkQueue _ _ Z) _ {prf} = void $ prf Refl
enqueuePrf (MkQueue {l} {m} front rear (S pad)) x =
  let q: BoundedQueue (l + (S m + pad)) a =
    rewrite plusAssociative l (S m) pad in
    MkQueue front (x :: rear) pad in
  let q: BoundedQueue (l + (m + S pad)) a =
    rewrite sym $ plusSuccRightSucc m pad in q in
  let q: BoundedQueue (l + m + S pad) a =
    rewrite sym $ plusAssociative l m (S pad) in q in
  q


export
enqueue : BoundedQueue n a -> a -> Maybe (BoundedQueue n a)
enqueue q x with (decIsFull q)
  | Yes _ = Nothing
  | No prf = Just $ enqueuePrf q x {prf}

enqueue' : (q: BoundedQueue n a) -> a -> Either (isFull q = True) (BoundedQueue n a)
enqueue' q x with (decIsFull q)
  | Yes prf = Left prf
  | No prf = Right $ enqueuePrf q x {prf}

export
isFullEnqueueNothing: (q: BoundedQueue n a) -> (pf: isFull q = True) -> (x: a) -> enqueue q x = Nothing
isFullEnqueueNothing (MkQueue _ _  Z   ) _ _ = Refl
isFullEnqueueNothing (MkQueue _ _ (S _)) p _ = void $ falseNotTrue p

export
notIsFullEnqueueIsJust: (q: BoundedQueue n a) -> (pf: Not (isFull q = True)) -> (x: a) -> IsJust $ enqueue q x
notIsFullEnqueueIsJust (MkQueue _ _  Z   ) contra _ = void $ contra (Refl)
notIsFullEnqueueIsJust (MkQueue _ _ (S _)) _      _ = ItIsJust

-- TODO: decide name
dequeuePrf : (q: BoundedQueue n a) -> {prf: Not (isEmpty q = True)} -> (a, BoundedQueue n a)
dequeuePrf (MkQueue [] [] pad) {prf} = void $ prf Refl
dequeuePrf {n=S l + m + pad} (MkQueue {l=S l} {m} (x :: xs) rear pad) =
  let q: BoundedQueue (l + m + (S pad)) a = MkQueue xs rear (S pad) in
  let q: BoundedQueue (S (l + m + pad)) a = rewrite plusSuccRightSucc (l + m) pad in q in
  (x, q)
dequeuePrf {n = S m + pad} (MkQueue {l=Z} {m=S m} [] rear pad) =
  let rear' = reverse rear in
  let e = head rear' in
  let front = tail rear' in
  let q: BoundedQueue (m + Z + S pad) a = MkQueue front [] (S pad) in
  let q: BoundedQueue (m +     S pad) a = rewrite sym $ plusZeroRightNeutral m in q in
  let q: BoundedQueue (S (m + pad))   a = rewrite plusSuccRightSucc m pad in q in
  (e, q)


export
dequeue : BoundedQueue n a -> (Maybe a, BoundedQueue n a)
dequeue q with (decIsEmpty q)
  | Yes _  = (Nothing, q)
  | No prf = let (x, q) = dequeuePrf q {prf} in (Just x, q)

export
dequeue' : (q: BoundedQueue n a) -> Either (isEmpty q = True) (a, BoundedQueue n a)
dequeue' q with (decIsEmpty q)
  | Yes prf = Left prf
  | No  prf = Right $ dequeuePrf q {prf}


export
head : BoundedQueue n a -> Maybe a
head = fst . dequeue

export
head' : (q: BoundedQueue n a) -> Either (isEmpty q = True) a
head' q =  map fst $ dequeue' q

export
isEmptyHeadNothing: (q: BoundedQueue n a) -> (pf: isEmpty q = True) -> head q = Nothing
isEmptyHeadNothing (MkQueue  []     []    _) _  = Refl
isEmptyHeadNothing (MkQueue (_::_)  _     _) pf = void $ falseNotTrue pf
isEmptyHeadNothing (MkQueue  []    (_::_) _) pf = void $ falseNotTrue pf
isEmptyHeadNothing (MkQueue (_::_) (_::_) _) pf = void $ falseNotTrue pf

export
notIsEmptyHeadIsJust: (q: BoundedQueue n a) -> (pf: Not (isEmpty q = True)) -> IsJust $ head q
notIsEmptyHeadIsJust (MkQueue [] [] p) contra = void $ contra Refl
notIsEmptyHeadIsJust (MkQueue (_::_) _ p) _ = ItIsJust
notIsEmptyHeadIsJust (MkQueue [] (r::rs) p) _ = ItIsJust

export
drop : BoundedQueue n a -> BoundedQueue n a
drop = snd . dequeue

||| head is front
export
fromVect : Vect n a -> BoundedQueue n a
fromVect {n} xs =
  rewrite sym $ plusZeroRightNeutral n in
  rewrite sym $ plusZeroRightNeutral (n + 0) in
  MkQueue xs [] 0

||| head is rear
export
fromVectReverse : Vect n a -> BoundedQueue n a
fromVectReverse {n} xs =
  rewrite sym $ plusZeroRightNeutral n in
  MkQueue [] xs 0

export
toVect: BoundedQueue n a -> (m ** Vect m a)
toVect {n = l + m + pad} (MkQueue {l} {m} f b pad) =
  (l + m ** f ++ reverse b)

export
toVectReverse: BoundedQueue n a -> (m ** Vect m a)
toVectReverse {n = l + m + pad} (MkQueue {l=l} {m=m} f b pad) =
  (m + l ** b ++ reverse f)

implementation (Eq a) => Eq (BoundedQueue n a) where
  (==) q1 q2 =
    let (n1 ** v1) = toVect q1 in
    let (n2 ** v2) = toVect q2 in
    case decEq n1 n2 of
      Yes pf => v1 == rewrite pf in v2
      No _   => False


revAppend : Vect m a -> Vect n a -> Vect (m + n) a
revAppend [] ys = ys
revAppend {m=S m'} {n = n} (x :: xs) ys =
  rewrite plusSuccRightSucc m' n in
  revAppend xs (x::ys)

export
append : (xs : BoundedQueue len1 a) -> (ys : BoundedQueue len2 a) -> BoundedQueue (len1 + len2) a
append (MkQueue {l=l} {m=m} front rear pad) (MkQueue {l=l'} {m=m'} front' rear' pad') =
  let newFront: Vect l a = front in
  let newRear: Vect (m' + (l' + m)) a = (rear' ++ (revAppend front' rear)) in
  let newRear: Vect (m + (l' + m')) a =
    rewrite plusAssociative m l' m' in
    rewrite plusCommutative (m + l') m' in
    rewrite plusCommutative m l' in
    newRear in
  let newPad = pad + pad' in
  let q: BoundedQueue (l + (m + (l' + m')) + (pad + pad')) a =  MkQueue newFront newRear newPad in
  let q: BoundedQueue ((l + m + pad) + (l' + m' + pad')) a =
    rewrite sym $ plusAssociative (l + m) pad (l' + m' + pad') in
    rewrite plusAssociative pad (l' + m') pad' in
    rewrite plusCommutative pad (l' + m') in
    rewrite sym $ plusAssociative (l' + m') pad pad' in
    rewrite plusAssociative (l + m) (l' + m') (pad + pad') in
    rewrite sym $ plusAssociative l m (l' + m') in
    q in
  q

export
reverse : BoundedQueue n a -> BoundedQueue n a
reverse (MkQueue {l} {m} front rear pad) =
  rewrite plusCommutative l m in
  MkQueue rear front pad

-- export
-- filter : (a -> Bool) -> BoundedQueue n a -> BoundedQueue n a
-- filter {n} f (MkQueue front rear _) =
--   let (l' ** front') = filter f front in
--   let (m' ** rear') = filter f rear in
--   MkQueue front' rear' (n - l' - m')
