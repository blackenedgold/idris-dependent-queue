module Queue
import Data.Vect

export
data Queue : Nat -> Type -> Type where
  MkQueue: {l, m: Nat} -> (front : Vect l a) -> (rear : Vect m a) -> Queue (l + m) a

%default total

export
empty : Queue 0 a
empty = MkQueue [] []

export
lengthZeroIsEmpty: (q: Queue 0 a) -> q = Queue.empty
lengthZeroIsEmpty (MkQueue [] []) = Refl

export
isEmpty : Queue n a -> Bool
isEmpty (MkQueue [] []) = True
isEmpty _ = False

export
emptyIsEmpty: isEmpty Queue.empty = True
emptyIsEmpty = Refl

export
decEmpty : Queue n a -> Dec (n = 0)
decEmpty (MkQueue [] []) = Yes (Refl)
decEmpty {n=S l + m} (MkQueue {l=S l} (_::_) _) = No SIsNotZ
decEmpty {n=l + S m} (MkQueue {m=S m} _ (_::_)) =
  rewrite sym $ plusSuccRightSucc l m in
  No SIsNotZ

export
length : Queue n a -> Nat
length (MkQueue front rear) = (length front) + (length rear)

export
enqueue : Queue n a -> a -> Queue (S n) a
enqueue (MkQueue {l} {m} front rear) x =
   rewrite plusSuccRightSucc l m in
   MkQueue front (x :: rear)

export
dequeue : Queue (S n) a -> (a, Queue n a)
dequeue (MkQueue (x :: xs) rear) = (x, MkQueue xs rear)
dequeue {n = n} (MkQueue [] rear) =
  rewrite sym (plusZeroRightNeutral n) in
  case reverse rear of
    e::front => (e, MkQueue front [])

export
dequeue' : Queue n a -> (Maybe a, Queue (pred n) a)
dequeue' {n = Z} xs = (Nothing, xs)
dequeue' {n = S n'} xs = let (a, xs) = dequeue xs in (Just a, xs)

export
head : Queue (S n) a -> a
head = fst . dequeue


export
head' : Queue n a -> Maybe a
head' = fst . dequeue'

export
drop : Queue (S n) a -> Queue n a
drop = snd . dequeue

||| head is front
export
fromVect : Vect n a -> Queue n a
fromVect {n = n} xs = rewrite sym (plusZeroRightNeutral n) in MkQueue xs []

||| head is rear
export
fromVectReverse : Vect n a -> Queue n a
fromVectReverse {n = n} xs = MkQueue [] xs

export
toVect: Queue n a -> Vect n a
toVect (MkQueue f b) = f ++ reverse b

export
toVectReverse: Queue n a -> Vect n a
toVectReverse (MkQueue {l=l} {m=m} f b) =
  rewrite plusCommutative l m in
  b ++ reverse f


implementation (Eq a) => Eq (Queue n a) where
  (==) q1 q2 = toVect q1 == toVect q2


revAppend : Vect m a -> Vect n a -> Vect (m + n) a
revAppend [] ys = ys
revAppend {m=S m'} {n = n} (x :: xs) ys =
  rewrite plusSuccRightSucc m' n in
  revAppend xs (x::ys)

export
append : (xs : Queue m a) -> (ys : Queue n a) -> Queue (m + n) a
append (MkQueue {l=l} {m=m} front rear) (MkQueue {l=l'} {m=m'} front' rear') =
  rewrite sym $ plusAssociative l m (l' + m') in
  rewrite plusCommutative m (l' + m') in
  rewrite plusCommutative l'  m' in
  rewrite sym $ plusAssociative m' l' m in
  MkQueue front (rear' ++ (revAppend front' rear))


export
reverse : Queue n a -> Queue n a
reverse (MkQueue {l} {m} front rear) =
  rewrite plusCommutative l m in
  MkQueue rear front

export
filter : (a -> Bool) -> Queue n a -> (n': Nat ** Queue n' a)
filter f (MkQueue front rear) =
  let (l' ** front') = filter f front in
  let (m' ** rear') = filter f rear in
  (l' + m' ** MkQueue front' rear')

